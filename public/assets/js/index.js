(function () {
  const module = {
    list: [],
    init: function () {
      this.cacheDom();
      this.bindingEvents();
      this.showTask();
    },
    cacheDom: function () {
      this.listTask = document.getElementById("tasks-list");
      this.button = document.getElementById("buttonAdd");
      this.input = document.getElementById("task");
      this.forn = document.getElementById("new-task");
    },
    bindingEvents: function () {
      this.button.addEventListener("click", this.addTask.bind(this));
    },
    addTask: function (event) {
      event.preventDefault();
      if (!this.input.value) {
        this.input.style.borderColor = "red";
        return;
      } else {
        if(this.isEdit){
          const indexList = this.list.findIndex(item => item.id === this.rowId)
          console.log(indexList)
         this.list[indexList].text=this.input.value;
         


          this.isEdit=false;
          this.button.innerText="Add"

        }else{
          const lastItemArray = this.list.slice(-1).pop();
        let lastNumberArray;
        if (lastItemArray) 
          lastNumberArray = lastItemArray.id.slice(-1);
        else 
          lastNumberArray = 0;

        this.list.push({
          text: this.input.value,
          id: `text-${parseInt(lastNumberArray) + 1}`,
        });
        }

        localStorage.setItem("misDatos", JSON.stringify(this.list));
        this.input.value = "";
        this.showTask();
      }
    },

    showTask: function () {
      this.listTask.innerHTML = "";
      const localStor = localStorage.getItem("misDatos");
      this.list = JSON.parse(localStor) || [];

      this.list.forEach((element) => {
        const li = document.createElement("li");
        const p = document.createElement("p");
        const btnDelete = document.createElement("button");
        const btnEdit = document.createElement("button");
        const iconoDelete = document.createElement("i");
        const iconoEdit = document.createElement("i");
        const input = document.createElement("input");
        p.setAttribute("id", "p-" + element.id);
        input.type = "checkbox";
        input.className = "Task-checkbox";
        input.addEventListener("change", this.checkTask.bind(this))
        li.classList.add("Task");
        btnDelete.className = "Task-delete";
        btnDelete.setAttribute("id", "delete-" + element.id);
        btnDelete.addEventListener("click", this.deleteTask.bind(this));
        btnEdit.className = "Task-edit";
        btnEdit.addEventListener("click", this.editTask.bind(this));
        btnEdit.setAttribute("id", "edit-" + element.id);
        btnDelete.appendChild(iconoDelete);
        btnEdit.appendChild(iconoEdit);
        iconoDelete.className = "fa-solid fa-trash-can";
        iconoEdit.className = "fa-solid fa-pen-to-square";
        p.innerText = element.text;
        li.appendChild(input);
        li.appendChild(p);
        li.appendChild(btnEdit);
        li.appendChild(btnDelete);
        this.listTask.appendChild(li);
      });
    },
    editTask: function (event) {
      const id = event.target.id;
      const textPosition = id.indexOf("text");
      const textFromId = id.slice(textPosition, id.length);
      const $p = document.getElementById("p-"+textFromId);
      console.log($p.textContent);
      this.input.value=$p.textContent;
      this.isEdit = true;
       this.rowId = textFromId;
       this.button.innerText = "Edit";
    },
    checkTask: function(event){
      const $checkbox = event.target;
      if (!$checkbox) return;
      
      if($checkbox.checked)
        event.target.nextElementSibling.style.textDecoration = "line-through";
      else 
        event.target.nextElementSibling.style.textDecoration = "unset";
    },
    deleteTask: function (event) {
      const id = event.target.id;
      const textPosition = id.indexOf("text");
      const textFromId = id.slice(textPosition, id.length)
      const newList = this.list.filter((item) => item.id !== textFromId.trim());

      localStorage.setItem("misDatos", JSON.stringify(newList));
      this.showTask();
    },
  };
  module.init();
})();
